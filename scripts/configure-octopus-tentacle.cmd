echo "Configuring Octopus tentacle instance..."
cd "C:Program FilesOctopus DeployTentacle"
  
Tentacle.exe create-instance --instance "Tentacle" --config "C:OctopusTentacleTentacle.config" --console
Tentacle.exe new-certificate --instance "Tentacle" --if-blank --console
Tentacle.exe new-squid --instance "Tentacle" --console
Tentacle.exe configure --instance "Tentacle" --reset-trust --console
Tentacle.exe configure --instance "Tentacle" --home "C:Octopus" --app "C:OctopusApplications" --port "10933" --console
Tentacle.exe configure --instance "Tentacle" --trust "YOUROCTOPUSKEYGOESHERE" --console
 
"netsh" advfirewall firewall add rule "name=Octopus Deploy Tentacle" dir=in action=allow protocol=TCP localport=10933
Tentacle.exe service --instance "Tentacle" --install --start --console
 
echo "Done!"